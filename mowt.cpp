#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <chrono>

using namespace std;

bool firstTime = true;
int currentWorkspace = 0;

string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (fgets(buffer, sizeof buffer, pipe) != NULL) {
            result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

void writeStringToFile(string text)
{
	ofstream workspaceLog;
	workspaceLog.open("/home/moritz/.workspace-log", ios::in | ios::app);
	workspaceLog << text;
	workspaceLog.close();
}

void writeToFile(int workspaceIndex)
{
	string text = ".";

	if (firstTime || currentWorkspace != workspaceIndex)
	{
		currentWorkspace = workspaceIndex;
		text = "\n" + to_string(workspaceIndex) + text;
		firstTime = false;
	}

  writeStringToFile(text);
}

void timer()
{
	while (true) {
		int workspaceIndex = stoi(exec("xprop -root -notype _NET_CURRENT_DESKTOP | cut -d' ' -f3"));
		writeToFile(workspaceIndex);
		sleep(60);
	}
}

int main()
{
	const auto p1 = chrono::system_clock::now();
	writeStringToFile("\n" + to_string(chrono::duration_cast<chrono::seconds>(p1.time_since_epoch()).count()));
	timer();
}
